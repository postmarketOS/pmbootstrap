# pmbootstrap

The pmbootstrap repository has been moved:
https://postmarketos.org/source-code/

Related blog post:
https://postmarketos.org/blog/2024/10/14/gitlab-migration/
